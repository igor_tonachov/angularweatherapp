angular
	.module('weatherApp')
	.factory('weatherService', [ '$http', 'localStorageService', function($http, localStorageService){
			return {
		getWeather: getWeather
	};

	function getWeather(cityName) {
		return $http.get('http://api.openweathermap.org/data/2.5/weather?q=' + cityName +'&APPID=9739aecdb2b607bbc1077c0938b75202'); 
	}
	
}]);



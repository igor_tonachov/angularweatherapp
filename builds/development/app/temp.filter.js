angular
	.module('weatherApp')
		.filter('kelvinToCelsius', function($filter){
			return function(kelvin) {
				return Math.round(kelvin-273.15);
			}; 
		});
angular
	.module('weatherApp', ['LocalStorageModule', 'ngRoute'])
		.config(['localStorageServiceProvider', function(localStorageServiceProvider){
			localStorageServiceProvider.setPrefix('weather');
		}])
		.config(function($routeProvider) {
	        $routeProvider

	            .when('/', {
	                templateUrl : 'builds/development/app/templates/weather.temp.html',
	                controller  : 'WeatherController'
	            })
	            .when('/city/:id', {
	            		templateUrl : 'builds/development/app/templates/detailedWeather.temp.html',
	            		controller: 'WeatherController'
	            });
	  })
	.controller ("WeatherController", [ '$scope', '$http', '$timeout', 'weatherService', 'localStorageService', '$routeParams',
	 function($scope, $http, $timeout, weatherService, localStorageService, $routeParams) {

		$scope.cityWeather = [
		];
		$scope.isFavorite = true;
		$scope.isError = false;
		$scope.search = '';
		$scope.cityName = '';

		$scope.addCity = addCity;
		$scope.removeItem = removeItem;
		$scope.showFavorite = showFavorite;
		$scope.addToFavorite = addToFavorite;

		$scope.$on('$routeChangeSuccess', function() { 
			$scope.weatherDetailed = $scope.cityWeather[$routeParams.id];
		})

		function addCity(cityName){
			getWeather(cityName);
		}

		function getWeather(cityName) {
			weatherService.getWeather(cityName).then(function(response){
				if (response.data.message) {
					console.log(response.data);
					$scope.isError = true;
					$timeout(function() {
						$scope.isError = false;
					}, 3000);
					return;
				}
				var localData = [];
				localData = localStorageService.get('cityWeatherData');
				if(localData !== 0) {
					angular.forEach(localData, function(loc, index){
						if(loc.name === response.data.name) {
							console.log(loc.name, response.data.name);
							localData.splice(index, 1);
						}
					});
					localData.push(response.data);
					localStorageService.set('cityWeatherData', localData);
				}else {
					localData.push(response.data);
					localStorageService.set('cityWeatherData', localData);
				}
				$scope.cityName = '';
				displayCity();
			})
		}

		function removeItem(cityId) {
			var localData = localStorageService.get('cityWeatherData');
			angular.forEach(localData, function(cityData, index) {
				if(cityData.id === cityId) {
					localData.splice(index, 1);
				}
			});
			localStorageService.set('cityWeatherData', localData);

			displayCity();
		}

		function showFavorite() {
			$scope.isFavorite = !$scope.isFavorite;
			var favoriteCities = []; 
			var localData = localStorageService.get('cityWeatherData');
			if($scope.isFavorite === true){
				angular.forEach(localData, function(cityData) {
					if(cityData.favorite === true) {
						favoriteCities.push(cityData);
					}
				});
				$scope.cityWeather = favoriteCities;
			} else {
				displayCity();
			}
		}

		function addToFavorite(cityId) {
			var localData = localStorageService.get('cityWeatherData');
			angular.forEach(localData, function(cityData, index) {
				if(cityData.id === cityId) {
					cityData.favorite = !cityData.favorite;
				}
			});
			localStorageService.set('cityWeatherData', localData);

			displayCity();
		}

		function displayCity() {
			$scope.cityWeather = localStorageService.get('cityWeatherData');
		}

		function updateWeather() {
			var citiesNames = [];
			var localData = localStorageService.get('cityWeatherData');
			if (localData !== 0) {
				angular.forEach(localData, function(loc) {
					citiesNames.push(loc.name);
					console.log(loc.name);
				});
				angular.forEach(citiesNames, function(city){
					getWeather(city);
				});
			} 
		}

		updateWeather();

		displayCity()
	}]);

